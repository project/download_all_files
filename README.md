# Download All Files

This module provides a field formatter for the field type `'file'`
called `Table of files with download all link` . The format which
will allow you to `"Download all files"` through single click.
The files will be zipped as single file.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/download_all_files).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/download_all_files).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

- Using composer 
  `composer require drupal/download_all_files`
- Downloading the tar.gz file and extracting it in modules/contrib
- On module's page, copy the `"tar.gz"` link under the Downloads section at
  the bottom of that page. Copy a module's URL for installing. Then paste that
  link on your site at /admin/modules/install in the URL box, then press
  Install.


## Configuration

1. Go to `"admin/modules"` and enable `"Download all files"` module;
2. Add file field on Bundles.
3. On the Manage display page you can choose to display
   `"Table of files with download all link"`.


## Maintainers

- Manoj K - [manojapare](https://www.drupal.org/u/manojapare)
- Rakesh James - [rakesh.gectcr](https://www.drupal.org/u/rakeshgectcr)
- Jaideep Singh Kandari - [JayKandari](https://www.drupal.org/u/jaykandari)

This project has been sponsored by:
- [Valuebound](https://www.drupal.org/valuebound)
- [QED42](https://www.drupal.org/qed42)