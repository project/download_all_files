<?php

namespace Drupal\download_all_files\Plugin\Field\FieldFormatter;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'file_download_all' formatter.
 *
 * @FieldFormatter(
 *   id = "file_download_all",
 *   label = @Translation("Table of files with download all link"),
 *   field_types = {
 *     "file",
 *     "image"
 *   }
 * )
 */
class DownloadAllFormatter extends EntityReferenceFormatterBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleList;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs an DownloadAllFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_list
   *   The module list.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, array $settings, $label, $view_mode, array $third_party_settings, RendererInterface $renderer, ModuleExtensionList $module_list, FileUrlGeneratorInterface $file_url_generator) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->renderer = $renderer;
    $this->moduleList = $module_list;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $plugin_id,
    $plugin_definition,
    $configuration['field_definition'],
    $configuration['settings'],
    $configuration['label'],
    $configuration['view_mode'],
    $configuration['third_party_settings'],
    $container->get('renderer'),
    $container->get('extension.list.module'),
    $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'use_description_as_link_text' => '',
      'details' => '',
      'details_state' => '',
      'details_title' => 'Download Files',
      'simple_theme' => '',
      'link_position' => '',
      'link_icon' => '',
      'link_title' => 'Download All',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['use_description_as_link_text'] = [
      '#title' => $this->t('Use description as link text'),
      '#description' => $this->t('Replace the file name by its description when available'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('use_description_as_link_text'),
    ];
    $elements['details'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide in the details'),
      '#default_value' => $this->getSetting('details'),
    ];
    $elements['details_state'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Details state (open - close)'),
      '#default_value' => $this->getSetting('details_state'),
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][details]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $elements['details_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title for details'),
      '#default_value' => $this->getSetting('details_title'),
      '#states' => [
        'visible' => [
          ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][details]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $elements['simple_theme'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Simple theme (only title and download link)'),
      '#default_value' => $this->getSetting('simple_theme'),
    ];
    $elements['link_position'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Download all link show in header'),
      '#default_value' => $this->getSetting('link_position'),
    ];
    $elements['link_icon'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display download all link icon'),
      '#default_value' => $this->getSetting('link_icon'),
    ];
    $elements['link_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title for link'),
      '#required' => TRUE,
      '#default_value' => $this->getSetting('link_title'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();
    $summary[] = $this->t('Text for download all link: @link_title', ['@link_title' => $settings['link_title']]);
    empty($settings['use_description_as_link_text']) ?: $summary[] = $this->t('Use description as link text');
    empty($settings['simple_theme']) ?: $summary[] = $this->t('Use Simple theme');
    empty($settings['link_icon']) ?: $summary[] = $this->t('Display download all link icon');
    empty($settings['link_position']) ?: $summary[] = $this->t('Download all link show in header');
    if (!empty($settings['details'])) {
      $state = empty($settings['details_state']) ? $this->t('open') : $this->t('close');
      $title = empty($settings['details_title']) ? $this->t('Download') : $settings['details_title'];
      $summary[] = $this->t('Details title: @title, @state.', [
        '@title' => $title,
        '@state' => $state,
      ]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();
    if ($files = $this->getEntitiesToView($items, $langcode)) {
      $rows = [];
      $module_name = $this->getPluginDefinition()['provider'];
      $path = $this->moduleList->getPath($module_name);
      $link = [
        '#type' => 'link',
        '#attributes' => ['class' => ['download-file-icon']],
        '#attached' => ['library' => ['download_all_files/theme']],
        '#title' => [
          '#theme' => 'image',
          '#uri' => '/' . $path . '/images/downloadIcon.svg',
        ],
      ];
      /** @var \Drupal\file\Entity\File $file */
      foreach ($files as $file) {
        if (!empty($settings['simple_theme'])) {
          $file_name = $file->getFilename();
          $file_description = $file->_referringItem->description;
          $table_link = $link;
          $table_link['#url'] = $this->fileUrlGenerator->generate($file->getFileUri());
          $table_link['#attributes']['download'] = $file_name;
          $rows[] = [
            [
              'data' => [
                '#prefix' => '<div class="download-file-name">',
                '#suffix' => '</div>',
                '#plain_text' => ($settings['use_description_as_link_text'] && !empty($file_description)) ? $file_description : $file_name,
              ],
            ],
            ['data' => $table_link],
          ];
        }
        else {
          $rows[] = [
            [
              'data' => [
                '#theme' => 'file_link',
                '#description' => $settings['use_description_as_link_text'] ? $file->_referringItem->description : NULL,
                '#file' => $file,
                '#cache' => [
                  'tags' => $file->getCacheTags(),
                ],
              ],
            ],
            ['data' => ByteSizeMarkup::create($file->getSize())],
          ];
        }
      }

      $elements[0] = [];

      if (!empty($rows)) {
        $field_name = $items->getName();
        /** @var \Drupal\Core\Entity\EntityInterface $entity */
        $entity = $items->getParent()->getEntity();
        $url = Url::fromRoute('download_all_files.download_path', [
          'entity_type' => $entity->getEntityTypeId(),
          'entity' => $entity->id(),
          'field_name' => $field_name,
        ]);
        if (!empty($settings['link_icon'])) {
          $download_all_files_link = $link;
          $download_all_files_link['#url'] = $url;
        }
        else {
          $download_all_files_link = Link::fromTextAndUrl($settings['link_title'], $url)
            ->toRenderable();
        }
        if (!empty($settings['link_position'])) {
          $header = [
            $settings['link_title'],
            $this->renderer->render($download_all_files_link),
          ];
        }
        elseif (!empty($settings['simple_theme'])) {
          $header = [$this->t('Name'), $this->t('Link')];
          $elements[]['download_all_files'] = $download_all_files_link;
        }
        else {
          $header = [$this->t('Attachment'), $this->t('Size')];
          $elements[]['download_all_files'] = $download_all_files_link;
        }
        $table = [
          '#theme' => 'table__file_formatter_table',
          '#header' => $header,
          '#rows' => $rows,
        ];
        if (!empty($settings['details'])) {
          $details_title = !empty($settings['details_title']) ? $settings['details_title'] : $this->t('Download');
          $elements[0] = [
            '#type' => 'details',
            '#title' => $details_title,
            '#open' => empty($settings['details_state']) ? TRUE : FALSE,
          ];
          $elements[0]['table'] = $table;
        }
        else {
          $elements[0] = $table;
        }
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function needsEntityLoad(EntityReferenceItem $item) {
    return parent::needsEntityLoad($item) && $item->isDisplayed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity) {
    // Only check access if the current file access control handler explicitly
    // opts in by implementing FileAccessFormatterControlHandlerInterface.
    $access_handler_class = $entity->getEntityType()->getHandlerClass('access');
    if (is_subclass_of($access_handler_class, '\Drupal\file\FileAccessFormatterControlHandlerInterface')) {
      return $entity->access('view', NULL, TRUE);
    }
    else {
      return AccessResult::allowed();
    }
  }

}
