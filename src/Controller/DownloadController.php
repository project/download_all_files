<?php

namespace Drupal\download_all_files\Controller;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\File\Event\FileUploadSanitizeNameEvent;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\download_all_files\Plugin\Archiver\Zip;
use Drupal\file\FileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Download files controller.
 *
 * @package Drupal\download_all_files\Controller
 */
class DownloadController extends ControllerBase {

  /**
   * Builds a DownloadController object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(
    protected FileSystemInterface $fileSystem,
    protected EventDispatcherInterface $eventDispatcher,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('file_system'),
      $container->get('event_dispatcher'),
    );
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, EntityInterface $entity): AccessResultInterface {
    return $entity->access('view', $account, TRUE);
  }

  /**
   * Method archive all file associated with entity and stream it for download.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   Entity file field name.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Downloads the file.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Archiver\ArchiverException
   */
  public function downloadAllFiles(Request $request, FieldableEntityInterface $entity, string $field_name): Response {
    if (!$entity->hasField($field_name)) {
      throw new NotFoundHttpException();
    }

    $zip_files_directory = $this->fileSystem->getTempDirectory() . '/daf_zips/' . $entity->getEntityTypeId();
    $file_path = $zip_files_directory . '/' . $this->sanitizeFilename($entity->id() . '-' . $entity->language()->getId() . '-' . $field_name . '-' . $this->currentUser()->id() . '.zip');
    $file_name = $this->sanitizeFilename($entity->label() . ' - ' . $field_name . '.zip');

    $files = [];
    // Construct zip archive and add all files, then stream it.
    $entity_field_files = $entity->get($field_name)->getValue();
    $file_storage = $this->entityTypeManager()->getStorage('file');
    foreach ($entity_field_files as $file) {
      $file_obj = $file_storage->load($file['target_id']);
      if ($file_obj instanceof FileInterface && $file_obj->access('view')) {
        $files[] = $file_obj->getFileUri();
      }
    }

    if (empty($files)) {
      $this->messenger()->addError('No files found for this entity to be downloaded', TRUE);
      return new RedirectResponse($request->server->get('HTTP_REFERER', '/'));
    }

    $zip_directory_exists = $this->fileSystem->prepareDirectory($zip_files_directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    if ($zip_directory_exists === FALSE) {
      $this->messenger()->addError('Zip file directory not found.', TRUE);
      return new RedirectResponse($request->server->get('HTTP_REFERER', '/'));
    }

    $file_zip = new Zip($file_path);
    foreach ($files as $file) {
      $file_zip->add($this->fileSystem->realpath($file));
    }

    $file_zip->close();

    $binary_file_response = new BinaryFileResponse($file_path);
    $binary_file_response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file_name);

    return $binary_file_response;
  }

  /**
   * Sanitizes the filename.
   *
   * @param string $filename
   *   The filename that should be sanitized.
   *
   * @return string
   *   The sanitized filename.
   */
  private function sanitizeFilename(string $filename): string {
    $event = new FileUploadSanitizeNameEvent(str_replace('/', '', $filename), 'zip');
    $this->eventDispatcher->dispatch($event);
    return $event->getFilename();
  }

}
